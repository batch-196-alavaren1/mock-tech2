let collection = [];
// Write the queue functions below.

function enqueue(element) {
    collection[collection.length] = element;
    return collection;
}
// visual:
// collection[0] = 'John'
// collection = ['John']
// collection[1] = 'Jane'
// collection = ['John', 'Jane']

function print() {
    return collection;
}


function dequeue() {
    let newCollection = [];

    for (let i = 1; i < collection.length; i++) {
        newCollection[i-1] = collection[i];

    }

    collection = newCollection;
    return collection;
}
// visual:
/*collection = ['John', 'Jane']
newCollection[1-1] = collection[1]
newCollection[0] = 'Jane'*/


function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
